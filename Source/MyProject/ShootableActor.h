// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShootableActor.generated.h"

UCLASS()
class MYPROJECT_API AShootableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShootableActor();
    void OnBulletHit();
    UPROPERTY(editAnywhere);
    UStaticMeshComponent* mVisibileComponent;
    UPROPERTY(editAnywhere);
    float jumpValue = 0.0f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void ChangeDirection();
	void ActorJump();
    FTimerHandle moveTimer;
    FTimerHandle jumpTimer;

    FVector mMovementInput;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
