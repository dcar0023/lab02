// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootableActor.h"


// Sets default values
AShootableActor::AShootableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    mVisibileComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
    mVisibileComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AShootableActor::BeginPlay()
{
	Super::BeginPlay();
	ChangeDirection();

    float nextJump = FMath::RandRange(1.0f, 10.0f);
    GetWorld()->GetTimerManager().SetTimer(jumpTimer, [this]() {
        ActorJump();
    }, nextJump, 1);
}

// Called every frame
void AShootableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (!mMovementInput.IsZero()) {
        mMovementInput.Normalize();
        SetActorLocation(GetActorLocation() + (mMovementInput * 100.0f * DeltaTime));
    }
    FVector NewLocation = GetActorLocation();
    if (NewLocation.Z > 400.0f) {
        jumpValue = -300.0f;
    } else if (NewLocation.Z < 200.0f && jumpValue < 0.0f) {
        jumpValue = 0.0f;
    }
    NewLocation.Z += jumpValue * DeltaTime;
    SetActorLocation(NewLocation);
}

void AShootableActor::OnBulletHit() {
    Destroy();
}

// Change the movement direction of the shootable target
void AShootableActor::ChangeDirection() {
    // set random x and y direction
    float randX = FMath::RandRange(-1.0f, 1.0f);
    float randY = FMath::RandRange(-1.0f, 1.0f);
    mMovementInput.X = randX;
    mMovementInput.Y = randY;

    // set random amount of time till it changes direction again
    int nextChange = FMath::RandRange(1,3);
    GetWorld()->GetTimerManager().SetTimer(moveTimer, [this]() {
        ChangeDirection();
    }, nextChange, 1);
}

void AShootableActor::ActorJump() {
    jumpValue = 300.0f;
    float nextJump = FMath::RandRange(1.0f, 10.0f);
    GetWorld()->GetTimerManager().SetTimer(jumpTimer, [this]() {
        ActorJump();
    }, nextJump, 1);
}

